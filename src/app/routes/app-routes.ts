import {Routes} from '@angular/router';

import {BodyComponent} from '../components/body/body.component';
import {FormUserDataComponent} from '../components/form-user-data/form-user-data.component';
import {ListUserDataComponent} from '../components/list-user-data/list-user-data.component';


import { ScrapingBachelorComponent } from '../components/scraping-bachelor/scraping-bachelor.component';

/*enlace en el footer*/


import {PageInformationComponent} from '../components/page-information/page-information.component';
import {SearchComponent} from '../components/search/search.component';
import {FavoritesHistoryComponent} from '../components/favorites-history/favorites-history.component';
import {HistoryComponent} from '../components/history/history.component';


export const routes: Routes = [

   // Body
   {path: '',                       component: BodyComponent},

   // FormUserData
   {path: 'form-user-data',         component: FormUserDataComponent},
   // {path: 'form-user-data/:id',     component: FormUserDataComponent},

   // ListUserData

   {path: 'list-user-data'       , component: ListUserDataComponent},
   {path: 'data-bachelor'       , component: ScrapingBachelorComponent},

   {path: 'list-user-data',      component: ListUserDataComponent},

   {path: 'list-user-data',         component: ListUserDataComponent},

   // Page information
   {path: 'page-information/:type',   component: PageInformationComponent},


   // Search
   {path: 'search',   component: SearchComponent},
   {path: 'search/:searchData',   component: SearchComponent},

   // Search
   {path: 'favorites',   component: FavoritesHistoryComponent},
   {path: 'favorites/:idFavorite',   component: FavoritesHistoryComponent},

   //history
   {path: 'history',   component: HistoryComponent},
   {path: 'history/:idHistory',   component: HistoryComponent},

];
