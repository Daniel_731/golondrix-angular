import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
   providedIn: 'root'
})
export class CRUDService {

   // Attributes

   private _API_ACCESS_POINT: string;

   private _HEADERS: HttpHeaders;


   // Constructor

   constructor(private httpClient: HttpClient) {
      // this.API_ACCESS_POINT = 'http://127.0.0.1:8000/api';
      this.API_ACCESS_POINT = 'https://www.back.golondrix.com/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1:8888/golondrix/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1/golondrix/public/api';

      this.HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
   }


   // CRUD methods

   create(table: string, apiToken: string) {
      try {

         return this.httpClient.get(this.API_ACCESS_POINT + `/user?api_token=${apiToken}`);

      } catch (e) {
         console.error(e);
      }
   }


   store(table: string, data: any) {
      try {

         return this.httpClient.post(this.API_ACCESS_POINT + '/' + table, data, {headers: this.HEADERS});

      } catch (e) {
         console.error(e);
      }
   }


   read(table: string) {
      try {

         return this.httpClient.get(this.API_ACCESS_POINT + '/' + table);

      } catch (e) {
         console.error(e);
      }
   }


   edit(table: string, id: number) {
      try {

         return this.httpClient.get(this.API_ACCESS_POINT + '/' + table + '/' + id + '/edit');

      } catch (e) {
         console.error(e);
      }
   }


   update(table: string, id: number, data: any) {
      try {

         return this.httpClient.put(this.API_ACCESS_POINT + '/' + table + '/' + id, data, {headers: this.HEADERS});

      } catch (e) {
         console.error(e);
      }
   }


   destroy(table: string, id: number) {
      try {

         return this.httpClient.delete(this.API_ACCESS_POINT + '/' + table + '/' + id);

      } catch (e) {
         console.error(e);
      }
   }


   // Getters && setters

   get API_ACCESS_POINT(): string {
      return this._API_ACCESS_POINT;
   }

   set API_ACCESS_POINT(value: string) {
      this._API_ACCESS_POINT = value;
   }

   get HEADERS(): HttpHeaders {
      return this._HEADERS;
   }

   set HEADERS(value: HttpHeaders) {
      this._HEADERS = value;
   }

}
