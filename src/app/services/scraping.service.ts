import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
   providedIn: 'root'
})
export class ScrapingService {

   private API_ACCESS_POINT: string;

   private HEADERS: HttpHeaders;

   constructor(private httpClient: HttpClient) {
      // this.API_ACCESS_POINT = 'http://localhost:8000/api';
      this.API_ACCESS_POINT = 'https://www.back.golondrix.com/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1:8888/golondrix/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1/golondrix/public/api';

      this.HEADERS = new HttpHeaders({'Content-Type': 'application/json'});

   }

   readScraping() {
      try {
         return this.httpClient.get(this.API_ACCESS_POINT + '/' + 'scraping_study_portals', {headers: this.HEADERS});

      } catch (e) {
         console.error(e);
      }
   }
}
