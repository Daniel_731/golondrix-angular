import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
   providedIn: 'root'
})
export class SendEmailService {

   // Attributes

   private _API_ACCESS_POINT: string;

   private _HEADERS: HttpHeaders;


   // Constructor

   constructor(private httpClient: HttpClient) {
      // this.API_ACCESS_POINT = 'http://127.0.0.1:8000/api';
      this.API_ACCESS_POINT = 'https://www.back.golondrix.com/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1:8888/golondrix/public/api';
      // this.API_ACCESS_POINT = 'http://127.0.0.1/golondrix/public/api';

      this.HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
   }


   // Send one email method

   sendEmail(data: any) {
      try {

         return this.httpClient.post(this.API_ACCESS_POINT + '/contact_form_send_email', data, {headers: this.HEADERS});

      } catch (e) {
         console.error(e);
      }
   }


   // Getters && setters

   get API_ACCESS_POINT(): string {
      return this._API_ACCESS_POINT;
   }

   set API_ACCESS_POINT(value: string) {
      this._API_ACCESS_POINT = value;
   }

   get HEADERS(): HttpHeaders {
      return this._HEADERS;
   }

   set HEADERS(value: HttpHeaders) {
      this._HEADERS = value;
   }

}
