import {Component, OnInit} from '@angular/core';
import counterUp from 'counterup2';
import {CRUDService} from '../../services/crud.service';


@Component({
   selector: 'app-body',
   templateUrl: './body.component.html',
   styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {

   // Attributes

   searchFieldData: string;
   idDiscipline: number;
   loginTypeNormal: boolean;
   discipline: Array<object> = [];
   fieldDisciplineKeyword = 'name_discipline';
   loggedIn = false;




   // Constructor

   constructor(private crudService: CRUDService) {
      this.loginTypeNormal = true;
      this.getAllDisciplines();

      this.authCheck();

   }

   // General methods

   ngOnInit() {
      document.querySelectorAll( '.counter' ).forEach(value => {
         counterUp(value, {
            duration: 1000,
            delay: 16,
         });
      });

      this.getAllDisciplines();
   }


   simulateClickSearchButton() {
      document.getElementById('search_button').click();
   }


   selectDisciplineEvent(item) {
      // this.searchFieldData = item.name_discipline;
      this.idDiscipline = item.id_discipline;
   }

   onClearedDiscipline() {
      // this.getAllDisciplines();

      // this.searchFieldData = null;
   }

   getAllDisciplines() {
      this.discipline = [];

      this.crudService.read('disciplines').subscribe((value) => {
         this.discipline = value[`discipline`];
      }, error => {
         console.error(error);
      });
   }

   authCheck() {
      if (localStorage.getItem('api_token')) {
         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {
            this.loggedIn  = true;
         }, error => {
            console.error(error);

            this.loggedIn  = false;
         });
      } else {
         this.loggedIn  = false;
      }
   }
}
