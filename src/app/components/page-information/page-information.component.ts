import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SendEmailService} from '../../services/send-email.service';

@Component({
   selector: 'app-page-information',
   templateUrl: './page-information.component.html',
   styleUrls: ['./page-information.component.scss']
})
export class PageInformationComponent implements OnInit {

   // Attributes
   _TYPE_INFORMATION: string;
   _CONTACT_FORM_DATA: object;
   _SHOW_CONTACT_BUTTON_SPINNER: boolean;
   _ALERT_TYPE: string;
   _ALERT_MESSAGE: string;
   _ALERT_SHOW: string;




   // Constructors

   constructor(private activatedRoute: ActivatedRoute, private sendEmailService: SendEmailService) {
      this.activatedRoute.params.subscribe(value => {
         window.scroll(0, 0);

         this.TYPE_INFORMATION = value.type;
      });
   }




   // General methods

   ngOnInit() {
      this.cleanContactForm();
   }



   sendFormContact() {
      this._SHOW_CONTACT_BUTTON_SPINNER = true;

      this.sendEmailService.sendEmail(this.CONTACT_FORM_DATA).subscribe(value => {
         this._SHOW_CONTACT_BUTTON_SPINNER = false;

         if (value[`status`]) {
            this.ALERT_TYPE = 'success';
            this.ALERT_MESSAGE = 'Los datos fueron enviados correctamente';
         } else {
            this.ALERT_TYPE = 'warning';
            this.ALERT_MESSAGE = 'Ocurrio un problema al enviar los datos, por favor intentelo mas tarde';
         }

         this.ALERT_SHOW = 'show';

         this.cleanContactForm();
         this.hideAlert();

      }, error => {
         this._SHOW_CONTACT_BUTTON_SPINNER = false;

         this.ALERT_TYPE = 'warning';
         this.ALERT_MESSAGE = 'Ocurrio un problema al enviar los datos, por favor intentelo mas tarde';
         this.ALERT_SHOW = 'show';

         this.cleanContactForm();
         this.hideAlert();
      });
   }



   cleanContactForm() {
      this.CONTACT_FORM_DATA = {
         name     : null,
         email    : null,
         comments : null
      };
   }



   hideAlert() {
      setTimeout(() => {
         this.ALERT_SHOW = '';
      }, 5000);
   }




   // Getters && setters

   get TYPE_INFORMATION(): string {
      return this._TYPE_INFORMATION;
   }

   set TYPE_INFORMATION(value: string) {
      this._TYPE_INFORMATION = value;
   }

   get CONTACT_FORM_DATA(): object {
      return this._CONTACT_FORM_DATA;
   }

   set CONTACT_FORM_DATA(value: object) {
      this._CONTACT_FORM_DATA = value;
   }

   get SHOW_CONTACT_BUTTON_SPINNER(): boolean {
      return this._SHOW_CONTACT_BUTTON_SPINNER;
   }

   set SHOW_CONTACT_BUTTON_SPINNER(value: boolean) {
      this._SHOW_CONTACT_BUTTON_SPINNER = value;
   }

   get ALERT_TYPE(): string {
      return this._ALERT_TYPE;
   }

   set ALERT_TYPE(value: string) {
      this._ALERT_TYPE = value;
   }

   get ALERT_MESSAGE(): string {
      return this._ALERT_MESSAGE;
   }

   set ALERT_MESSAGE(value: string) {
      this._ALERT_MESSAGE = value;
   }

   get ALERT_SHOW(): string {
      return this._ALERT_SHOW;
   }

   set ALERT_SHOW(value: string) {
      this._ALERT_SHOW = value;
   }
}
