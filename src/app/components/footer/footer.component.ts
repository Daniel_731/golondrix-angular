import {Component, OnInit} from '@angular/core';

@Component({
   selector: 'app-footer',
   templateUrl: './footer.component.html',
   styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

   showContainerCookiesMessage = false;




   constructor() {}




   ngOnInit() {
      const messageCookies = localStorage.getItem('message_cookies');

      if (!messageCookies) {
         this.showContainerCookiesMessage = true;
      }
   }



   acceptCookies() {
      localStorage.setItem('message_cookies', 'true');

      this.showContainerCookiesMessage = false;
   }

}
