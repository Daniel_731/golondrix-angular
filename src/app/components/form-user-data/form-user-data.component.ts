import {Component, OnInit, ViewChild} from '@angular/core';
import {UserData} from '../../interfaces/user_data';
import {WorkExperience} from '../../interfaces/work_experience';
import {Countries} from '../../interfaces/countries';
import {Cities} from '../../interfaces/cities';
import {CRUDService} from '../../services/crud.service';
import {Router} from '@angular/router';
import {PreviousStudes} from '../../interfaces/previous_studes';
import {TestUsuario} from '../../interfaces/test';
import {industries} from '../../data/industries';
import {language} from '../../data/language';

declare var jQuery: any;
declare var $: any;

@Component({
   selector: 'app-form-user-data',
   templateUrl: './form-user-data.component.html',
   styleUrls: ['./form-user-data.component.scss']
})
export class FormUserDataComponent implements OnInit {

   @ViewChild('field_autocomplete_universities', null) fieldAutocompleteUniversities;
   @ViewChild('field_autocomplete_discipline', null) fieldAutocompleteDiscipline;


   // Attributes
   selectedDate: any;
   showOrHideFilters = false;


   industries;
   language;
   endYeard: [];

   userData: UserData = {
      user_name: null,
      email: null,
      password: null,
      avatar: 'AVATAR',
      first_name: null,
      last_name: null,
      birth_day: null,
      gender: null,
      id_location: 1,
      biography_users: null,
      study_year: null,
      user_bachelor: null,
      user_master: null,
      user_phd: null,
      user_chortcourse: null,
   };

   experience: WorkExperience = {
      id_client: null,
      employer_name: null,
      job_title: null,
      start_date: null,
      id_location: null,
      id_countri: null,
      end_date: null,
      user_id: null,
      industry_job: null,
      name: null,
   };


   workExperi: WorkExperience;
   workExperiList: WorkExperience[];

   testUsu: TestUsuario;

   testUsuList: TestUsuario[];

   testUsuario: TestUsuario = {
      user_id: null,
      type_test: null,
      score: null,
      type_test2: null,
      core2: null,
   };
   /*
      minDate: {
         "year": 1900,
         "month": 1,
         "day": 1
       }*/

   minDate = {year: 1900, month: 1, day: 1};


   birthDate: object;
   startDate: object;
   endDate: object;


   previousStudes: PreviousStudes;
   previousStudesList: PreviousStudes[];

   languajeStudies: object = {};
   languajeStudiesList: [] = [];

   countries: Countries[] = [];
   states: [] = [];
   cities: Cities[] = [];

   id: number;
   idCity: number;
   idCountry: number;
   idState: number;

   editForm: boolean;


   years: Array<number> = [];
   yearSelected: number;


   showForm = 'container_user';


   alert: object = {
      type: null,
      message: null,
      show: null
   };


   disciplines: Array<object> = [];
   fieldDisciplinesKeyword = 'name_discipline';


   universities: Array<object> = [];
   idUniversity: number;
   nameUniversities = '';
   fieldUniversitiesKeyword = 'name_institution';


   peEndYearCurrent = false;
   weEndDateCurrent = false;


   // Constructor

   constructor(private crudService: CRUDService, private router: Router) {
      this.authCheck();
      this.readCountries();
      this.readAll();
      this.resetPreviousStudes();
      this.chargeYears();
      this.workExperience();
      this.readAllExperience();
      this.readAllTest();
      this.resetTest();
      this.getAllDisciplines();
      this.getAllUniversities(true);

      this.industries = industries;
      this.language = language;
   }

   clearAutocomplete() {
      try {

         this.fieldAutocompleteUniversities.close();
         this.fieldAutocompleteDiscipline.close();

         this.fieldAutocompleteUniversities.clear();
         this.fieldAutocompleteDiscipline.clear();

      } catch (e) {}
   }

   authCheck() {
      if (localStorage.getItem('api_token')) {
         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {
            this.id = value[`id`];

            this.edit();
            this.readAllExperience();
            this.readAll();
            this.readAllLanguage();
            this.readAllTest();

            this.resetPreviousStudes();
            this.resetTest();
            this.workExperience();
            this.resetLanguaje();
         }, error => {
            console.error(error);

            this.router.navigate(['/']);
         });

      } else {
         this.router.navigate(['/']);
      }
   }

   // General methods

   ngOnInit(): void {
      $('.dropify').dropify();
      this.resetUserData();
      this.workExperience();
   }


   chargeYears() {
      let currentYear = new Date().getFullYear();

      if (!this.userData.study_year) {
         this.userData.study_year = currentYear;
      }

      for (let i = 0; i < 6; i++) {
         this.years.push(currentYear++);
      }
   }


   resetUserData() {
      this.userData = {
         user_name: null,
         email: null,
         password: null,
         avatar: 'AVATAR',
         first_name: null,
         last_name: null,
         birth_day: null,
         id_location: 1,
         biography_users: null,
         user_bachelor: null,
         user_master: null,
         user_phd: null,
         user_chortcourse: null,
      };


      // console.log("..."+ this.selectedDate);
   }

   workExperience() {
      this.experience = {
         id_client: null,
         employer_name: null,
         job_title: null,
         start_date: null,
         id_location: null,
         id_countri: null,
         end_date: null,
         user_id: this.id,
         industry_job: null,
         name: null,
      };
   }


   resetPreviousStudes() {
      this.previousStudes = {
         university_user: null,
         degree_user: null,
         field_user: null,
         id_country: null,
         end_user: null,
         id_user: this.id,
      };
   }


   resetTest() {
      this.testUsu = {
         type_test: null,
         score: null,
         type_test2: null,
         core2: null,
         user_id: this.id,
      };

   }

   resetLanguaje() {

      this.languajeStudies = {
         language_name: null,
         language_level: null,
         id_user: this.id
      };

   }


   uploadAvatar() {
      document.getElementById('upload-avatar').click();
   }

   functionChange() {

      const input = document.getElementById('upload-avatar')
      if (input[`files`] && input[`files`][0]) {
         const reader = new FileReader();

         reader.onload = (e) => {

            this.userData.avatar = e.target[`result`];

         };

         reader.readAsDataURL(input[`files`][0]);
      }
   }

   readCountries() {
      this.crudService.read('countries').subscribe((value) => {
         this.countries = value[`countries`] as Countries[];
      }, error => {
         console.error(error);
      });
   }

   getAllStates() {

      this.states = [];
      this.cities = [];

      this.crudService.edit('states', this.idCountry).subscribe(value => {
         // console.log(value[`states`]);
         this.states = value[`states`];
      }, error => {
        // this.showAlert('danger', 'An error occurred');

         console.error(error);
      });
   }

   getAllCities() {

      this.crudService.edit('cities/city_by_state', this.idState).subscribe(value => {
         // console.log(value[`cities`]);
         this.cities = value[`cities`];
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }
   /* Detail User */

   async createAll(table: string, data: any, resetMethod, saveContinue?: string) {

      if (table === 'user_data/previous_studies' && this.peEndYearCurrent) {
         data.end_user = new Date().getFullYear();
      }


      if (table === 'user_data/previous_studies' && this.peEndYearCurrent) {
         data.end_user = new Date().getFullYear();
      }

      console.log(data);

      this.crudService.store(table, data).subscribe(value => {
         console.log(value);

         this.showAlert('success', 'Your information has been saved');
         this.clearAutocomplete();
         this.readAll();
         this.readAllLanguage();
         this.readAllExperience();
         this.readAllTest();

         if (saveContinue) {
            this.showForm = saveContinue;
         }
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   createAllExperie() {

      const startDate = new Date(this.startDate[`year`], this.startDate[`month`] - 1, this.startDate[`day`]);

      if (this.weEndDateCurrent) {
         const currentDate = new Date();
         currentDate.setDate(currentDate.getDate() - 1);

         this.experience.end_date = currentDate;
      } else {
         this.experience.end_date = new Date(this.endDate[`year`], this.endDate[`month`] - 1, this.endDate[`day`]);
      }

      this.experience.start_date = startDate;
      this.experience.user_id = this.id;

      // console.log(this.experience);

      this.crudService.store('user_data/work_experience', this.experience).subscribe(value => {

         this.showAlert('success', 'Your information has been saved');

         this.workExperience();
         this.readAllExperience();
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   readAll() {
      this.crudService.read('user_data/previous_studies/' + this.id).subscribe(value => {
         this.previousStudesList = value[`data`];
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   readAllExperience() {
      this.crudService.read('user_data/work_experience/' + this.id).subscribe(value => {
         this.workExperiList = value[`data`];
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   readAllTest() {
      this.crudService.read('user_data/test_user/' + this.id).subscribe(value => {
         this.testUsuList = value[`data`];
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   destroyAllExperience(id: number) {
      if (confirm('Are you sure you want to delete this record?')) {
         this.crudService.destroy('user_data/work_experience', id).subscribe((value) => {
            this.showAlert('success', 'Your record has been removed.');
            this.readAllExperience();
         }, error => {
            this.showAlert('danger', 'An error occurred');
            console.error(error);
         });
      }
   }

   destroyTest(id: number) {
      if (confirm('Are you sure you want to delete this record?')) {
         this.crudService.destroy('user_data/test_user', id).subscribe((value) => {
            this.showAlert('success', 'Your record has been removed.');
            this.readAllTest();
         }, error => {
            this.showAlert('danger', 'An error occurred');
            console.error(error);
         });
      }
   }
   /*Previous Languaje */

   readAllLanguage() {
      this.crudService.read('user_data/language_user/' + this.id).subscribe(value => {
         this.languajeStudiesList = value[`data`];
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   destroyAllLanguage(id: number) {
      if (confirm('Are you sure you want to delete this record?')) {
         this.crudService.destroy('user_data/language_user', id).subscribe((value) => {
            this.showAlert('success', 'Your record has been removed.');
            this.readAllLanguage();
         }, error => {
            this.showAlert('danger', 'An error occurred');
            console.error(error);
         });
      }
   }

   destroyAll(id: number) {
      if (confirm('Are you sure you want to delete this record?')) {
         this.crudService.destroy('user_data/previous_studies', id).subscribe((value) => {
            this.showAlert('success', 'Your record has been removed.');
            this.readAll();
         }, error => {
            this.showAlert('danger', 'An error occurred');
            console.error(error);
         });
      }
   }

   /*Detail user  */

   // CRUD

   create() {
      this.crudService.store('user_data', this.userData).subscribe(value => {
         this.showAlert('success', 'Your information has been saved');
         // this.resetUserData();

         localStorage.setItem('api_token', value[`api_token`]);
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   edit() {
      this.crudService.edit('user_data', this.id).subscribe(async value => {

         this.userData = value[`user_data`][0] as UserData;
         // console.log(this.userData.birth_day);
         const birthDatee = new Date(this.userData.birth_day);

         this.birthDate = {
            year: birthDatee.getFullYear(),
            month: birthDatee.getMonth() + 1,
            day: birthDatee.getDate()
         };

         // this.selectedDate = this.userData.birth_day;
         console.log(this.userData.id_client);


         if (this.userData.id_location) {
            this.idCountry = this.userData[`city`][0][`country_id`];

            await this.getAllStates();

            this.idState = this.userData[`city`][0][`state_id`];

            await this.getAllCities();

            this.idCity = this.userData[`city`][0][`id`];

         }

         this.readAllExperience();

      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   update() {
      this.userData.birth_day = new Date(this.birthDate[`year`], this.birthDate[`month`] - 1, this.birthDate[`day`] + 1);

      this.crudService.update('user_data', this.id, this.userData).subscribe(value => {
         this.showAlert('success', 'Your information has been saved');

         this.showForm = 'container_previous_studies';
      }, error => {
         this.showAlert('danger', 'An error occurred');
         console.error(error);
      });
   }

   showAlert(type: string, message: string) {
      this.alert[`type`] = type;
      this.alert[`message`] = message;
      this.alert[`show`] = 'show';

      setTimeout(() => {
         this.alert[`show`] = '';
      }, 5000);
   }
   // Disciplines

   getAllDisciplines() {
      this.crudService.read('disciplines').subscribe((value) => {
         for (const disciplines of value[`discipline`]) {
            this.disciplines.push(disciplines);
         }

         console.log(value);
      }, error => {
         console.error(error);
      });
   }

   selectDisciplineEvent(item) {
      this.previousStudes.degree_user = item.name_discipline;
   }

   onClearedDiscipline() {
      this.previousStudes.degree_user = null;
   }

   // Universities

   getAllUniversities(resetList: boolean) {
      if (resetList) {
         this.universities = [];
      }

      const filters = {
         filters : {
            name_universities    : this.nameUniversities,
         }
      };

      this.crudService.store('institutions/read_limit', filters).subscribe((value) => {
         for (const university of value[`universities`]) {
            this.universities.push(university);
         }

         // console.log(this.universities);
      }, error => {
         console.error(error);
      });
   }

   selectUniversitiesEvent(item) {
      this.previousStudes.university_user = item.name_institution;
   }

   onClearedUniversities() {
      this.idUniversity = null;
      this.previousStudes.university_user = null;
      this.getAllUniversities(true);
   }

   onChangeSearchUniversities(item) {
      this.nameUniversities = item;

      this.getAllUniversities(true);
   }

}
