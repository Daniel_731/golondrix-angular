import {Component, OnInit} from '@angular/core';
import {CRUDService} from '../../services/crud.service';
import {UserData} from '../../interfaces/user_data';

@Component({
   selector: 'app-list-user-data',
   templateUrl: './list-user-data.component.html',
   styleUrls: ['./list-user-data.component.scss']
})
export class ListUserDataComponent implements OnInit {

   users: UserData[] = [];



   constructor(private crudService: CRUDService) {
      this.read();
   }

   ngOnInit() {}



   read() {
      this.crudService.read('user_data').subscribe((value) => {
         this.users = value['user_data'] as UserData[];
      }, error => {
         console.error(error);
      });
   }

   previous(){

   }

   destroy(id: number) {
      if (confirm('Esta seguro que desea eliminar este registro?')) {
         this.crudService.destroy('user_data', id).subscribe((value) => {
            alert('El registro fue eliminado satisfactoriamente');

            this.read();
         });
      }
   }

}
