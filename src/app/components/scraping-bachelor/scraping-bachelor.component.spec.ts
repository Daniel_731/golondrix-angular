import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrapingBachelorComponent } from './scraping-bachelor.component';

describe('ScrapingBachelorComponent', () => {
  let component: ScrapingBachelorComponent;
  let fixture: ComponentFixture<ScrapingBachelorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrapingBachelorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapingBachelorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
