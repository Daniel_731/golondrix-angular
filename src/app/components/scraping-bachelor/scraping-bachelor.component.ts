import {Component, OnInit} from '@angular/core';
import {ScrapingService} from 'src/app/services/scraping.service';

@Component({
   selector: 'app-scraping-bachelor',
   templateUrl: './scraping-bachelor.component.html',
   styleUrls: ['./scraping-bachelor.component.scss']
})
export class ScrapingBachelorComponent implements OnInit {

   coursesData = [];


   constructor(private scrapingService: ScrapingService) {
      this.readScrapingBalechor();
   }

   ngOnInit() {
   }

   readScrapingBalechor() {


      this.scrapingService.readScraping().subscribe((value) => {
         this.coursesData = value[`disciplines`];
      }, error => {
         console.error(error);
      });
   }

}
