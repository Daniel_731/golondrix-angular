import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CRUDService} from '../../services/crud.service';
import {AuthService} from 'angularx-social-login';
import {SocialUser} from 'angularx-social-login';
import {UserData} from '../../interfaces/user_data';
import {GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider} from 'angularx-social-login';

declare var jQuery: any;
declare var $: any;

@Component({
   selector: 'app-login-form',
   templateUrl: './login-form.component.html',
   styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

   // Attributes

   @Output() masterAuthCheck = new EventEmitter();

   @Input('loginTypeNormal') masterLoginTypeNormal: boolean;
   @Input('loggedIn') masterLoggedIn: boolean;
   @Input('avatar') masterAvatar: string;
   @Input('idUser') masterIdUser: number;

   @ViewChild('field_name_one', null) fieldNameOne: ElementRef;
   @ViewChild('field_email_two', null) fieldEmailTwo: ElementRef;

   user: SocialUser;
   userData: UserData;
   repeatPassword: string;
   showPassword: boolean;
   typeLogin: number;

   ALERT_TYPE: string;
   ALERT_MESSAGE: string;
   ALERT_SHOW: string;
   TYPE_GOOGLE = 3;
   TYPE_FACEBOOK = 2;
   TYPE_NORMAL = 1;

   // Constructors

   constructor(private authService: AuthService, private crudService: CRUDService) {
   }


   // General methods

   ngOnInit() {
      this.resetUserData();

      this.showPassword = false;


      const modal = $('#exampleModal');

      modal.on('hidden.bs.modal', (e) => {
         this.resetUserData();
         this.showPassword = false;
         this.masterLoginTypeNormal = true;
      });

      modal.on('shown.bs.modal', (e) => {
         if (this.masterLoginTypeNormal) {
            this.fieldNameOne.nativeElement.focus();
         } else {
            this.fieldEmailTwo.nativeElement.focus();
         }
      });
   }


   signInWithGoogle(): void {
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(async response => {
         this.user = response;

         this.typeLogin = this.TYPE_GOOGLE;

         console.log(response);
         await this.setLoginData();
         this.create();
        // this.loginRedSociales();
      }).catch(reason => {
         console.error(reason);
      });
   }


   signInWithFB(): void {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(async response => {
         this.user = response;
         this.typeLogin = this.TYPE_FACEBOOK;
         console.log(response);

         await this.setLoginData();
         // this.loginRedSociales();
         this.create();

      }).catch(reason => {
         console.error(reason);
      });
   }


   signInWithLIN(): void {
      try {
         this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID).then(response => {

            this.user = response;

            console.log(this.user);

            // if (this.user) {
            this.setLoginData();
            this.create();
            //  }

            // window.location.reload();
         }).catch(reason => {
            console.error(reason);
         });
      } catch (e) {
         console.error(e);

      }
   }


   signOut(): void {
      this.authService.signOut().then(r => {
      });
   }


   setLoginData() {
      this.userData = {
         user_name: this.user.name,
         email: this.user.email,
         password: this.user.email,
         avatar: this.user.photoUrl,
         first_name: this.user.firstName,
         last_name: this.user.lastName,
         birth_day: null,
         id_location: 1
      };
   }


   resetUserData() {
      this.userData = {
         user_name: null,
         email: null,
         password: null,
         avatar: null,
         first_name: null,
         last_name: null,
         birth_day: null,
         id_location: null,
      };
   }


   login() {

      if (this.validateFormLogin()) {

         this.crudService.store('user_data/login', this.userData).subscribe(value => {
            if (value && value[`status`]) {
               $('#exampleModal').modal('hide');

               this.masterLoggedIn = true;

               localStorage.setItem('api_token', value[`api_token`]);

               this.resetUserData();

               this.masterAuthCheck.emit();

               window.location.reload();

            } else {
               if (!this.masterLoginTypeNormal) {
                  this.userData.password = null;
                  this.fieldEmailTwo.nativeElement.focus();

                  this.showAlert('warning', 'The username or password is incorrect');
               }
            }
         }, error => {
            console.error(error);
         });
      } else {

         this.showAlert('info', 'The username and password are mandatory');
      }
   }

   create(typeNormal?: boolean) {

      if (typeNormal) {
         this.typeLogin = this.TYPE_NORMAL;
      }

      this.ALERT_SHOW = '';

      if (this.validateFormLogin()) {
         this.userData.id_client = this.typeLogin;

         this.crudService.store('user_data', this.userData).subscribe(value => {

            if (!value[`status`] && value[`error`] && value[`error`] === 1 ) {
               this.showAlert('warning', 'Please, check if your information is correct or if you already have an account with us.');

               return;
            }

            if (!value[`status`] && value[`error`] === 2 ) {
               this.showAlert('warning', 'The email entered is not valid.');

               return;
            }

            if (value[`first_user`]) {
               // $('#modal_notification').modal('show');

               localStorage.setItem('first_user', 'true');
            }

            if (value && value[`status`]) {
               $('#exampleModal').modal('hide');

               this.masterLoggedIn = true;

               localStorage.setItem('api_token', value[`api_token`]);

               this.resetUserData();

               this.masterAuthCheck.emit();

               window.location.reload();

            }

         }, error => {
            console.error(error);
         });
      } else {

         this.showAlert('info', 'The username and password are mandatory');
      }
   }



   validateFormLogin(): boolean {
      try {

         // tslint:disable-next-line:max-line-length
         return (this.userData.email !== null && this.userData.email !== '') && (this.userData.password !== null && this.userData.password !== '');

      } catch (e) {
         console.error(e);
      }
   }


   showAlert(type: string, message: string) {
      this.ALERT_TYPE = type;
      this.ALERT_MESSAGE = message;
      this.ALERT_SHOW = 'show';

      this.hideAlert();
   }


   hideAlert() {
      setTimeout(() => {
         this.ALERT_SHOW = '';
      }, 5000);
   }


   showLogIn() {
      $('#exampleModal').modal('hide');
      $('#modal_log_in').modal('show');
   }


   showJoinForFree() {
      $('#modal_log_in').modal('hide');
      $('#exampleModal').modal('show');
   }

}
