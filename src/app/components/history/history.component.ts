import {Component, OnInit} from '@angular/core';
import {CRUDService} from '../../services/crud.service';
import {ActivatedRoute} from '@angular/router';

@Component({
   selector: 'app-history',
   templateUrl: './history.component.html',
   styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

   // Attributes

   idUser: number;
   histories: Array<object> = [];
   selectedHistory: number = null;

   showNotResults = false;
   showOrHideFilters = false;


   // Constructor

   constructor(private crudService: CRUDService, private activatedRoute: ActivatedRoute) {
      this.selectedHistory = this.activatedRoute.snapshot.params.idHistory;
   }

   // General methods

   ngOnInit() {
      this.authCheck();
   }

   getHistoriesByIdUser(resetFavorite: boolean) {
      try {

         this.showNotResults = false;

         this.crudService.edit('history', this.idUser).subscribe(value => {
            if (resetFavorite) {
               this.selectedHistory = null;
            }

            this.histories = value[`histories`];

            this.showNotResults = true;
         }, error => {
            console.log(error);
         });

      } catch (e) {
         console.error(e);
      }
   }


   authCheck() {
      if (localStorage.getItem('api_token')) {
         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {
            this.idUser = value[`id`];

            this.getHistoriesByIdUser(false);
         }, error => {
            console.error(error);
         });
      }
   }


   deleteHistory(history: object) {

      console.log(history);

      if (this.selectedHistory === history[`id_history`]) {
         this.selectedHistory = null;
      }

      console.log(this.selectedHistory);

      this.crudService.destroy('history', history[`id_history`]).subscribe(value => {
         this.getHistoriesByIdUser(true);
      }, error => {
         console.error(error);
      });
   }


   courseEdxFormatURL(url: string): string {
      return url.replace('page-data\/', '').replace('\/page-data\.json', '');
   }

}
