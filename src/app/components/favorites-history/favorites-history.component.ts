import {Component, OnInit} from '@angular/core';
import {CRUDService} from '../../services/crud.service';
import {ActivatedRoute} from '@angular/router';

@Component({
   selector: 'app-favorites-history',
   templateUrl: './favorites-history.component.html',
   styleUrls: ['./favorites-history.component.scss']
})
export class FavoritesHistoryComponent implements OnInit {

   // Attributes

   idUser: number;
   favorites: Array<object> = [];
   selectedFavorite: number = null;

   showNotResults = false;
   showOrHideFilters = false;




   // Constructor

   constructor(private crudService: CRUDService, private activatedRoute: ActivatedRoute) {
      this.selectedFavorite = this.activatedRoute.snapshot.params.idFavorite;
   }




   // General methods

   ngOnInit() {
      this.authCheck();
   }



   getFavoritesByIdUser(resetFavorite: boolean) {
      try {

         this.showNotResults = false;

         this.crudService.edit('favorites', this.idUser).subscribe(value => {
            if (resetFavorite) {
               this.selectedFavorite = null;
            }

            this.favorites = value[`favorites`];

            console.log(value, this.favorites);

            this.showNotResults = true;
         }, error => {
            console.log(error);
         });

      } catch (e) {
         console.error(e);
      }
   }



   authCheck() {
      if (localStorage.getItem('api_token')) {
         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {
            this.idUser = value[`id`];

            this.getFavoritesByIdUser(false);
         }, error => {
            console.error(error);
         });
      }
   }



   deleteFavorite(favorite: object) {
      this.crudService.destroy('favorites', favorite[`id`]).subscribe(value => {
         this.getFavoritesByIdUser(true);
      }, error => {
         console.error(error);
      });
   }



   courseEdxFormatURL(url: string): string {
      return url.replace('page-data\/', '').replace('\/page-data\.json', '');
   }

}
