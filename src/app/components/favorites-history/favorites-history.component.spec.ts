import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritesHistoryComponent } from './favorites-history.component';

describe('FavoritesHistoryComponent', () => {
  let component: FavoritesHistoryComponent;
  let fixture: ComponentFixture<FavoritesHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritesHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
