import {Component, OnInit, ViewChild} from '@angular/core';
import {CRUDService} from '../../services/crud.service';
import {ActivatedRoute} from '@angular/router';
import {Courses} from '../../interfaces/courses';
import {SendEmailService} from '../../services/send-email.service';

declare var jQuery: any;
declare var $: any;

@Component({
   selector: 'app-search',
   templateUrl: './search.component.html',
   styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

   // Attributes

   @ViewChild('field_autocomplete_disciplines', null) fieldAutocompleteDisciplines;
   @ViewChild('field_autocomplete_countries', null) fieldAutocompleteCountries;
   @ViewChild('field_autocomplete_states', null) fieldAutocompleteStates;
   @ViewChild('field_autocomplete_cities', null) fieldAutocompleteCities;
   @ViewChild('field_autocomplete_universities', null) fieldAutocompleteUniversities;


   loggedIn = false;
   idUser: number;

   value: number;
   min: number;
   max: number;
   step: number;

   courses: Array<Courses> = [];

   countResults: number;

   disciplines: Array<object> = [];
   idDiscipline: number;
   nameDiscipline = '';

   countries: Array<object> = [];
   idCountry: number;
   nameCountry: string;

   states: Array<object> = [];
   idState: number;
   nameState = '';

   cities: Array<object> = [];
   idCity: number;
   nameCity = '';

   universities: Array<object> = [];
   idUniversity: number;
   nameUniversity = '';

   arrayData: {};
   searchFieldData: string;
   showSeeMoreComment = false;
   initialPage = 1;

   skills: string;

   typeCourses: string;

   fieldDisciplinesKeyword = 'name_discipline';
   fieldCountriesKeyword = 'name';
   fieldStatesKeyword = 'name';
   fieldCitiesKeyword = 'name';
   fieldUniversitiesKeyword = 'name_institution';

   textAreaSendMessage: string;

   CONTACT_FORM_DATA: object;
   SHOW_CONTACT_BUTTON_SPINNER: boolean;
   ALERT_TYPE: string;
   ALERT_MESSAGE: string;
   ALERT_SHOW: string;

   showLoading: boolean;
   showNotResults: boolean;


   showOrHideFilters = false;




   // Constructor

   constructor(private crudService: CRUDService, private activatedRoute: ActivatedRoute, private sendEmailService: SendEmailService) {
      this.activatedRoute.params.subscribe(value => {
         this.searchFieldData = value.searchData;
      });
   }




   // General methods

   read(resetList: boolean) {

      this.showLoading = true;
      this.showNotResults = false;

      if (resetList) {
         this.courses = [];
         this.initialPage = 1;
      }

      const filters = {
         filters : {
            // tslint:disable-next-line:max-line-length
            field_data     : this.searchFieldData ? (this.searchFieldData[`name_discipline`] ? this.searchFieldData[`name_discipline`] : this.searchFieldData) : null,
            field_check    : this.arrayData,
            field_fee      : this.value,
            initial_page   : this.initialPage,
            name_country   : this.nameCountry,
            name_state     : this.nameState,
            name_city      : this.nameCity,
            id_institution : this.idUniversity,
            skills         : this.skills,
            type_course    : this.typeCourses,
            id_user        : this.idUser,
            min_price      : this.min,
            max_price      : this.max
         }
      };

      this.crudService.store('curso-search', filters).subscribe((value) => {

         if (value && value[`courses`]) {

            for (const course of value[`courses`]) {
               if (course[`url_curso`]) {
                  course[`url_curso`] = course[`url_curso`].replace(/page-data\/|\/page-data.json/g, '');
               }

               this.courses.push(course);
            }


            if (value[`count_results`] && value[`count_results`][`results`]) {
               this.countResults = value[`count_results`][`results`];
            } else {
               this.countResults = null;
            }


            if (value[`courses`]) {
               this.initialPage += value[`courses`].length;
            }

            this.showNotResults = value[`courses`].length === 0;
         }

         this.showLoading = false;
      }, error => {
         // console.error(error);

         this.showLoading = false;
      });
   }



   cleanFilters() {
      try {

         this.searchFieldData = null;
         this.value = 300000;
         this.initialPage = 1;
         this.idState = null;
         this.idCountry = null;
         this.idUniversity = null;
         this.skills = null;
         this.typeCourses = null;


         this.arrayData = {
            fullTime    : false,
            partTime    : false,

            bachelor    : false,
            master      : false,
            phd         : false,
            shortCourse : false,
            free        : false,

            type_order  : null,

            face        : false,
            online      : false,
            blended     : false,
            lessDay     : false,
            lessWeek    : false,
            lessMonth   : false,
            lessYear    : false,
            year1       : false,
            year2       : false,
            year4       : false
         };

         this.fieldAutocompleteDisciplines.close();
         this.fieldAutocompleteCountries.close();
         this.fieldAutocompleteStates.close();
         this.fieldAutocompleteCities.close();
         this.fieldAutocompleteUniversities.close();

         this.fieldAutocompleteDisciplines.clear();
         this.fieldAutocompleteCountries.clear();
         this.fieldAutocompleteStates.clear();
         this.fieldAutocompleteCities.clear();
         this.fieldAutocompleteUniversities.clear();

         this.cleanContactForm();
         this.read(true);

      } catch (e) {
         console.error(e);
      }
   }



   onScroll() {
      if (!this.showNotResults) {
         this.read(false);
      }
   }



   ngOnInit() {
      this.min = 0;
      this.max = 300000;
      this.value = 300000;
      this.step = 1;

      this.arrayData = {
         fullTime    : false,
         partTime    : false,

         bachelor    : false,
         master      : false,
         phd         : false,
         shortCourse : false,
         free        : false,

         type_order  : null,

         face        : false,
         online      : false,
         blended     : false,
         lessDay     : false,
         lessWeek    : false,
         lessMonth   : false,
         lessYear    : false,
         year1       : false,
         year2       : false,
         year4       : false
      };


      this.getAllDisciplines();
      this.getAllCountries();
      this.getAllUniversities(true);
      this.cleanContactForm();
      this.authCheck();
   }



   onClickSaveButton(course: Courses) {
      try {

         if (this.idUser) {

            const favorite = {
               id_user     : this.idUser,
               id_course   : course.id_course
            };

            this.crudService.store('favorites', favorite).subscribe(value => {
               if (value[`status`]) {
                  course[`id_favorite`] = value[`id_favorite`];

                  this.ALERT_TYPE = 'success';
                  this.ALERT_MESSAGE = 'The course was added to your favorites.';

               } else {

                  this.ALERT_TYPE = 'warning';
                  this.ALERT_MESSAGE = 'There was a problem adding the course to your favorites';
               }

               this.ALERT_SHOW = 'show';

               this.hideAlert();

            }, error => {

               this.ALERT_TYPE = 'warning';
               this.ALERT_MESSAGE = 'There was a problem adding the course to your favorites';
               this.ALERT_SHOW = 'show';

               this.hideAlert();
            });

         } else {

            $('#exampleModal').modal('show');
         }

      } catch (e) {
         console.error(e);
      }
   }



   onClickSaveHistoryButton(course: Courses) {
      try {

         if (this.idUser) {
            course[`showSeeMore`] = !course[`showSeeMore`];

            const history = {
               id_user     : this.idUser,
               id_course   : course.id_course
            };

            this.crudService.store('history', history).subscribe(value => {
               if (value[`status`]) {
                  course[`id_history`] = value[`id_history`];
               }

            }, error => {
               // console.error(error);
            });
         } else {
            $('#exampleModal').modal('show');
         }

      } catch (e) {
         console.error(e);
      }
   }



   allOrOnlineCourses(type: string) {
      this.typeCourses = type;

      this.read(true);
   }



   authCheck() {
      if (localStorage.getItem('api_token')) {

         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {

            this.CONTACT_FORM_DATA[`email`]  = value[`email`];
            this.CONTACT_FORM_DATA[`name`]   = value[`user_name`];

            this.loggedIn  = true;
            this.idUser    = value[`id`];


            this.onScroll();

         }, error => {
            this.onScroll();
         });

      } else {
         this.onScroll();
      }
   }



   sendFormContact() {
      this.SHOW_CONTACT_BUTTON_SPINNER = true;

      this.sendEmailService.sendEmail(this.CONTACT_FORM_DATA).subscribe(value => {
         this.SHOW_CONTACT_BUTTON_SPINNER = false;

         if (value[`status`]) {
            this.ALERT_TYPE = 'success';
            this.ALERT_MESSAGE = 'Your message was successfully sent.';
         } else {
            this.ALERT_TYPE = 'warning';
            this.ALERT_MESSAGE = 'There was a problem sending the message.';
         }

         this.ALERT_SHOW = 'show';

         this.cleanContactForm();
         this.hideAlert();

         this.showSeeMoreComment = false;

      }, error => {
         this.SHOW_CONTACT_BUTTON_SPINNER = false;

         this.ALERT_TYPE = 'warning';
         this.ALERT_MESSAGE = 'There was a problem sending the message.';
         this.ALERT_SHOW = 'show';

         this.cleanContactForm();
         this.hideAlert();

         this.showSeeMoreComment = false;
      });
   }



   cleanContactForm() {
      this.CONTACT_FORM_DATA = {
         name     : null,
         email    : null,
         comments : null
      };
   }



   hideAlert() {
      setTimeout(() => {
         this.ALERT_SHOW = '';
      }, 5000);
   }



   // Disciplines

   getAllDisciplines() {
      this.crudService.read('disciplines').subscribe((value) => {
         for (const disciplines of value[`discipline`]) {
            this.disciplines.push(disciplines);
         }
      }, error => {
         // console.error(error);
      });
   }



   selectDisciplineEvent(item) {
      this.searchFieldData = item.name_discipline;

      this.read(true);
   }



   onClearedDiscipline() {
      this.searchFieldData = null;
      this.read(true);
   }



   onFocusedDiscipline(event) {
      this.fieldAutocompleteCountries.close();
      this.fieldAutocompleteStates.close();
      this.fieldAutocompleteCities.close();
      this.fieldAutocompleteUniversities.close();
   }




   // Countries

   getAllCountries() {
      this.crudService.read('countries').subscribe((value) => {
         for (const country of value[`countries`]) {
            this.countries.push(country);
         }
      }, error => {
         // console.error(error);
      });
   }



   selectCountryEvent(item) {
      this.idCountry = item != null ? item.id : null;
      this.idState = null;
      this.idCity = null;

      this.nameCountry = item != null ? item.name : null;
      this.nameCity = null;
      this.nameState = null;

      this.read(true);
      this.getAllStates(true);

      if (item.id !== 233) {
         this.getAllCitiesByCountry(true);
      }
   }



   onClearedCountries() {
      this.idCountry = null;
      this.idState = null;
      this.idCity = null;

      this.nameCountry = null;
      this.nameCity = null;
      this.nameState = null;

      this.fieldAutocompleteStates.clear();
      this.fieldAutocompleteStates.close();

      this.fieldAutocompleteCities.clear();
      this.fieldAutocompleteCities.close();

      this.states = [];
      this.cities = [];

      this.read(true);
   }



   onFocusedCountries(event) {
      this.fieldAutocompleteDisciplines.close();
      this.fieldAutocompleteStates.close();
      this.fieldAutocompleteCities.close();
      this.fieldAutocompleteUniversities.close();
   }




   // States

   getAllStates(resetList: boolean) {
      if (resetList) {
         this.states = [];
      }

      this.crudService.edit('states', this.idCountry).subscribe((value) => {
         for (const city of value[`states`]) {
            this.states.push(city);
         }
      }, error => {
         // console.error(error);
      });
   }



   selectStatesEvent(item) {
      this.idState = item != null ? item.id : null;
      this.idCity = null;

      this.nameState = item != null ? item.name : null;
      this.nameCity = null;

      this.read(true);
      this.getAllCities(true);
   }



   onClearedStates() {
      this.idState = null;
      this.idCity = null;

      this.nameState = null;
      this.nameCity = null;

      if (this.idCountry === 233) {
         this.fieldAutocompleteCities.clear();
         this.fieldAutocompleteCities.close();

         this.cities = [];
      }

      this.read(true);
   }



   onChangeSearchStates(item) {}



   onFocusedStates(event) {
      this.fieldAutocompleteDisciplines.close();
      this.fieldAutocompleteCountries.close();
      this.fieldAutocompleteCities.close();
      this.fieldAutocompleteUniversities.close();
   }




   // Cities

   getAllCities(resetList: boolean) {
      if (resetList) {
         this.cities = [];
      }

      this.crudService.edit('cities', this.idState).subscribe((value) => {
         for (const city of value[`cities`]) {
            this.cities.push(city);
         }
      }, error => {
         // console.error(error);
      });
   }


   getAllCitiesByCountry(resetList: boolean) {
      if (resetList) {
         this.cities = [];
      }

      this.crudService.read('cities/read_limit/' + this.idCountry).subscribe((value) => {
         console.log(value);

         if (value && value[`cities`]) {
            for (const city of value[`cities`]) {
               this.cities.push(city);
            }
         }
      }, error => {
         // console.error(error);
      });
   }



   selectCitiesEvent(item) {
      this.idCity = item != null ? item.id : null;
      this.nameCity = item != null ? item.name : null;

      this.read(true);
   }



   onClearedCities() {
      this.idCity = null;
      this.nameCity = null;

      this.read(true);
   }



   onChangeSearchCities(item) {}



   onFocusedCities(event) {
      this.fieldAutocompleteDisciplines.close();
      this.fieldAutocompleteCountries.close();
      this.fieldAutocompleteUniversities.close();
   }




   // Universities

   getAllUniversities(resetList: boolean) {
      if (resetList) {
         this.universities = [];
      }

      const filters = {
         filters : {
            name_universities : this.nameUniversity,
         }
      };

      this.crudService.store('institutions/read_limit', filters).subscribe((value) => {
         for (const university of value[`universities`]) {
            this.universities.push(university);
         }
      }, error => {
         // console.error(error);
      });
   }



   selectUniversitiesEvent(item) {
      this.idUniversity = item != null ? item.id_institution : null;

      this.read(true);
   }



   onClearedUniversities() {
      this.idUniversity = null;
      this.nameUniversity = null;

      this.read(true);
      this.getAllUniversities(true);
   }



   onChangeSearchUniversities(item) {
      this.nameUniversity = item;

      this.getAllUniversities(true);
   }



   onFocusedUniversities(event) {
      this.fieldAutocompleteDisciplines.close();
      this.fieldAutocompleteCountries.close();
      this.fieldAutocompleteStates.close();
      this.fieldAutocompleteCities.close();
   }




   createHistory(course) {
      try {

         if (this.idUser) {

            const history = {
               id_user     : this.idUser,
               id_course   : course.id_course
            };

            this.crudService.store('history', history).subscribe(value => {
               if (value[`status`]) {
                  // course[`id_favorite`] = value[`id_favorite`];

                  this.ALERT_TYPE = 'success';
                  this.ALERT_MESSAGE = 'The course was added to your history.';

               } else {

                  this.ALERT_TYPE = 'warning';
                  this.ALERT_MESSAGE = 'There was a problem adding the course to your history';
               }

               this.ALERT_SHOW = 'show';

               this.hideAlert();

            }, error => {

               this.ALERT_TYPE = 'warning';
               this.ALERT_MESSAGE = 'There was a problem adding the course to your history';
               this.ALERT_SHOW = 'show';

               this.hideAlert();
            });

         } else {

            $('#exampleModal').modal('show');
         }

      } catch (e) {
         console.error(e);
      }
   }



   onFocusedKeyWords() {
      this.fieldAutocompleteDisciplines.close();
      this.fieldAutocompleteCountries.close();
      this.fieldAutocompleteStates.close();
      this.fieldAutocompleteCities.close();
      this.fieldAutocompleteUniversities.close();
   }

}
