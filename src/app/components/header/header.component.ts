import {Component, OnInit} from '@angular/core';
import {AuthService} from 'angularx-social-login';
import {SocialUser} from 'angularx-social-login';
import {CRUDService} from '../../services/crud.service';

declare var jQuery: any;
declare var $: any;

@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

   // Attributes

   loginTypeNormal: boolean;
   user: SocialUser;
   loggedIn: boolean;
   countries: object[];
   currency: string;
   country: string;
   avatar: string;
   idUser: number;
   userName: string;


   // Constructors

   constructor(private authService: AuthService, private crudService: CRUDService) {
      this.loginTypeNormal = true;

      this.setCurrency(localStorage.getItem('currency') ? localStorage.getItem('currency') : 'USD');
      this.setCountry(localStorage.getItem('country') ? localStorage.getItem('country') :  'United States');
   }


   // General methods

   ngOnInit() {
      this.authCheck();
      this.getAllCountries();
   }


   signOut(): void {
      localStorage.removeItem('api_token');

      this.loggedIn = false;
      this.avatar = null;
      this.idUser = null;

      try {
         this.authService.signOut();
      } catch (e) {}

      window.location.reload();
   }


   getAllCountries() {
      this.crudService.read('countries').subscribe((value) => {
         this.countries = value[`countries`];
      }, error => {
         console.error(error);
      });
   }



   setCurrency(currency) {
      if (currency && currency !== '') {
         this.currency = currency;
         localStorage.setItem('currency', currency);
      } else {
         this.currency = 'USD';
         localStorage.setItem('currency', 'USD');
      }
   }



   setCountry(country) {
      if (country && country !== '') {
         this.country = country;
         localStorage.setItem('country', country);
      } else {
         this.country = 'United States';
         localStorage.setItem('country', 'United States');
      }
   }



   authCheck() {
      if (localStorage.getItem('api_token')) {
         this.crudService.create('user_data', localStorage.getItem('api_token')).subscribe((value) => {
            this.loggedIn = true;
            this.avatar = value[`avatar`];
            this.idUser = value[`id`];
            this.userName = value[`user_name`];

            if (localStorage.getItem('first_user')) {
               const selector = $('#modal_notification');

               selector.modal('show');

               selector.on('hidden.bs.modal', (e) => {
                  localStorage.removeItem('first_user');
               });

               // localStorage.removeItem('first_user');
            }
         }, error => {
            console.error(error);
         });
      }
   }

}
