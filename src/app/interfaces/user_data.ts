export interface UserData {
   id?: number;
   id_client?: number;
   user_name: string;
   email: string;
   password: string;
   avatar: string;
   first_name: string;
   last_name: string;
   birth_day: Date;
   gender?: string;
   status?: string;
   id_location: number;
   remember_token?: string;
   created_at?: string;
   updated_at?: string;
   biography_users?: string;
   study_year?: number;
   user_bachelor?: boolean;
   user_master?: boolean;
   user_phd?: boolean;
   user_chortcourse?: boolean;
}
