export interface WorkExperience {

    id?: number;
    id_client?: number;
    employer_name?: string;
    job_title?: string;
    start_date?: Date;
    id_location?: number;
    id_countri?: number;
    end_date?: Date;
    user_id?: number;
    industry_job?: string;
    name: string;
}
