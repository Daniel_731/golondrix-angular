export interface PreviousStudes {
   id_previous?: number;
   id_user?: number;
   university_user?: string;
   degree_user: string;
   field_user?: string;
   id_country?: number;
   end_user?: number;
}
