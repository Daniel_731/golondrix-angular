export interface Courses {
   id_course?: number;
   id_discipline: number;
   name_course: string;
   description_course: string;
   type?: string;
   category?: string;
   duration?: string;
   url?: string;
   status?: string;
   density_parttime?: number;
   density_fulltime?: number;
   methods_face2face?: number;
   methods_online?: number;
   methods_blended?: number;
   id_institution?: number;
   id_city?: number;
   tuition_fee_value?: number;
   tuition_fee_unit?: string;
   tuition_fee_currency?: string;
   codigo_curso?: number;
   url_curso?: string;
}
