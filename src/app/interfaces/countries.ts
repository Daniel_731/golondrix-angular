export interface Countries {
   id: string;
   name: string;
   iso3: string;
   iso2: string;
   phonecode: string;
   capital: string;
   currency: string;
   created_at: string;
   updated_at: string;
}
