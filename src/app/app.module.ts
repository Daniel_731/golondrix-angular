import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {routes} from './routes/app-routes';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {BodyComponent} from './components/body/body.component';
import {FooterComponent} from './components/footer/footer.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {FormUserDataComponent} from './components/form-user-data/form-user-data.component';
import {ListUserDataComponent} from './components/list-user-data/list-user-data.component';

import {ScrapingBachelorComponent} from './components/scraping-bachelor/scraping-bachelor.component';


import {PageInformationComponent} from './components/page-information/page-information.component';

import {SocialLoginModule, AuthServiceConfig, LoginOpt} from 'angularx-social-login';
import {GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider} from 'angularx-social-login';
import {SearchComponent} from './components/search/search.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {NgAutoCompleteModule} from 'ng-auto-complete';
import { FavoritesHistoryComponent } from './components/favorites-history/favorites-history.component';
import { HistoryComponent } from './components/history/history.component';
//import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';







const fbLoginOptions: LoginOpt = {
   scope: 'email',
   return_scopes: true,
   enable_profile_selector: true
};


const googleLoginOptions: LoginOpt = {
   scope: 'profile email'
};


const linkedinLoginOptions: LoginOpt = {
   scope: 'profile email'
};


const config = new AuthServiceConfig([
   {
      id: GoogleLoginProvider.PROVIDER_ID,
     // provider: new GoogleLoginProvider('627716139459-lsjqj5u9neme8acgkap1fkvdm1vsjr8s.apps.googleusercontent.com')
      provider: new GoogleLoginProvider('627716139459-66ho7qpf3ff5fjbqj1ikfp2hmrt6efq0.apps.googleusercontent.com')
      
   },
   {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('2099429800363350')
   },
   {
      id: LinkedInLoginProvider.PROVIDER_ID,
      provider: new LinkedInLoginProvider('78izr0uyt9xya7')
   }
]);

export function provideConfig() {
   return config;
}

@NgModule({
   declarations: [
      AppComponent,
      HeaderComponent,
      BodyComponent,
      FooterComponent,
      LoginFormComponent,
      FormUserDataComponent,
      ListUserDataComponent,
      ScrapingBachelorComponent,
      PageInformationComponent,
      SearchComponent,
      FavoritesHistoryComponent,
      HistoryComponent,
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      RouterModule.forRoot(routes, {useHash : true}),
      SocialLoginModule,
      InfiniteScrollModule,
      AutocompleteLibModule,
      NgAutoCompleteModule,
      NgbModule
   ],
   providers: [
      {
         provide: AuthServiceConfig,
         useFactory: provideConfig
      }
   ],
   bootstrap: [AppComponent]
})
export class AppModule {
}
